//
// Created by vojta on 09.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_PREDMET_H
#define ZOO_PROJEKT_HRA_PREDMET_H

#include <iostream>

class Predmet {
protected:
    float m_hmotnost;
    std::string m_nazev;
public:
    Predmet(int hmotnost, std::string nazev);
    virtual float getHmotnost() = 0;
    virtual std::string getNazev() = 0;
    virtual void printInfo() = 0;

};


#endif //ZOO_PROJEKT_HRA_PREDMET_H
