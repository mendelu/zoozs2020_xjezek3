//
// Created by vojta on 10.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_SOUBOJ_H
#define ZOO_PROJEKT_HRA_SOUBOJ_H

#include "HlavniPostava.h"
#include "VedlejsiPostava.h"
#include <iomanip>

class Souboj {
public:
    static void souboj(HlavniPostava* hlavniPostava, VedlejsiPostava* vedlejsiPostava);
    static void printMoznosti();
    static void popisKola(int choice, HlavniPostava* hlavniPostava, VedlejsiPostava* vedlejsiPostava);


};


#endif //ZOO_PROJEKT_HRA_SOUBOJ_H
