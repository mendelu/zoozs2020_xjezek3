//
// Created by vojta on 15.12.2020.
//

#include "Lokace.h"

HlavniPostava *hlavniPostava = new HlavniPostava("Emil", 100, 50, 12);
Inventar *emiluvInventar = new Inventar(50);

Lokace::Lokace(int cisloLokace, std::string popis) {
    m_cisloLokace = cisloLokace;


}

void Lokace::sebratNesebratPredmet(Predmet *predmet) {
    std::cout << " 1. Sebrat " << predmet->getNazev() << " (ulozit do inventare)" << "\n" <<
              " 2. Nechat ho tam, kde lezi" << "\n" << "\n" <<
              " Tvoje volba: " << "\n";
    int choice;
    std::cin >> choice;
    if (choice == 1) {
        emiluvInventar->pridejPredmet(predmet);
        std::cout << "Sebral jsi : " << predmet->getNazev() << "\n";
        std::cout << "Stav tveho inventare je: " << emiluvInventar->getRozsah() << "\n";
        emiluvInventar->vypisInventar();
    } else if (choice == 2) {
        std::cout << "\n"
                  << "Vim, kam tim miris, ale ver mi, neni to dobrej napad. Takze tvoje podvedomi te ovladlo a klic sebralo za tebe"
                  << "\n";
        emiluvInventar->pridejPredmet(predmet);
        std::cout << "Stav tve inventare je: " << emiluvInventar->getRozsah() << "\n";
        emiluvInventar->vypisInventar();
    }
    system("pause");
}

void Lokace::otevriNeotevri1(Predmet *predmet) {
    int volba;
    std::cin >> volba;
    if (volba == 1) {
        std::cout << "Otevrel jsi dvere" << "\n";
    } else if (volba == 2) {
        std::cout << "Aha. Rozhodl ses pockat az te ty stvury najdou, gratuluji. " <<
                  "THE END!" << "\n";
    }
}


void Lokace::vstupDoLokaceUvodni() {
    Pribeh::printPribehZacatekUvod1();
    system("pause");
}


void Lokace::vstupDoLokace1(HlavniPostava *hlavniPostava) {
    VedlejsiPostava *premenenyStraznik = new VedlejsiPostava("Premeneny straznik", 85, 25, 10);
    ZbranANastroj *strep = new ZbranANastroj(5, "Ostry strep", 10);
    Vystroj *kevlar = new Vystroj(8, "Kevlarova vesta", 15);
    Item *klicOdCely = new Item(2, "Klic od tve cely", "Tento klic ti otevre svoji celu, a muzes se tak dostat ven");


    VedlejsiPostava *nepritel1 = new VedlejsiPostava("nepritel-test", 50, 70, 20);
    Pribeh::printPribehZacatekUvod2();
    sebratNesebratPredmet(klicOdCely);
    Pribeh::printPribehZacatekUvod3();
    Lokace::otevriNeotevri1(klicOdCely);
    Pribeh::printPribehZacatekUvod4();
    Lokace::sebratNesebratPredmet(strep);
    Souboj::souboj(hlavniPostava, premenenyStraznik);
    hlavniPostava->getHp();

}