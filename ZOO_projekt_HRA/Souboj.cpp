//
// Created by vojta on 10.12.2020.
//

#include "Souboj.h"


void Souboj::printMoznosti() {
    std::cout << "1. Lehky utok" << "\n" <<
              "2. Tezky utok" << "\n" <<
              "3. Vynechat kolo" << "\n" <<
              "\n" << "Tva volba: " << "\n";
}

void Souboj::popisKola(int choice, HlavniPostava *hlavniPostava, VedlejsiPostava *vedlejsiPostava) {
    if (choice == 1) {
        std::cout << "zavolal jsi lehky utok." << "\n" <<
                  "Zpusobil jsi mu: " << hlavniPostava->getUtok() << " DMG." << "\n" <<
                  "Nepritelovy zivoty: " << vedlejsiPostava->getHp() << " HP." << "\n" <<
                  "Nepritel na tebe zautocil lehkym utokem: " <<
                  "Dostavas poskozeni ve vysi: " << vedlejsiPostava->getUtok() << " DMG." << "\n" <<
                  "Soucasne zdravi: " << hlavniPostava->getHp() << " HP." << "\n";
    } else if (choice == 2) {
        std::cout << "zavolal jsi tezky utok." << "\n" <<
                  "Zpusobil jsi mu: " << 2 * hlavniPostava->getUtok() << " DMG." << "\n" <<
                  "Nepritelovy zivoty: " << vedlejsiPostava->getHp() << " HP." << "\n" <<
                  "Nepritel na tebe zautocil tezkym utokem: " <<
                  "Dostavas poskozeni ve vysi: " << 2 * vedlejsiPostava->getUtok() << " DMG." << "\n" <<
                  "Soucasne zdravi: " << hlavniPostava->getHp() << " HP." << "\n";

    } else if (choice == 3) {
        std::cout << "Rozhodl ses vynechat kolo" << "." <<
                  "Dostavas poskozeni ve vysi: " << vedlejsiPostava->getUtok() << " DMG." << "\n" <<
                  "Soucasne zdravi: " << hlavniPostava->getHp() << " HP." << "\n";
    }
}

void Souboj::souboj(HlavniPostava *hlavniPostava, VedlejsiPostava *vedlejsiPostava) {
    int choice;
    while ((hlavniPostava->getHp() > 0) and (vedlejsiPostava->getHp() > 0)) {
        std::cout << "\n" << "|-----FIGHT!!!-----|" << "\n";
        hlavniPostava->printInfo();
        vedlejsiPostava->printInfo();
        printMoznosti();
        std::cin >> choice;
        if (choice == 1) {
            vedlejsiPostava->uberZivotyLehky(hlavniPostava);
            hlavniPostava->uberZivotyLehky(vedlejsiPostava);
            popisKola(1, hlavniPostava, vedlejsiPostava);
        } else if (choice == 2) {
            vedlejsiPostava->uberZivotyTezky(hlavniPostava);
            hlavniPostava->uberZivotyTezky(vedlejsiPostava);
            popisKola(2, hlavniPostava, vedlejsiPostava);
        } else if (choice == 3) {
            hlavniPostava->uberZivotyLehky(vedlejsiPostava);
            popisKola(3, hlavniPostava, vedlejsiPostava);
        }


    }
    /*std::cout << "Chces hrat znovu ? " << "\n" <<
              "1. Ano" << "\n" <<
              "2. Ne" << "\n" << "\n" <<
              "Tvoje volba: " << "\n";
    int volba;
    std::cin >> volba;
    if (volba == 1) {
        return souboj(hlavniPostava, vedlejsiPostava);

    }
     */
}