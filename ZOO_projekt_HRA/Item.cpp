//
// Created by vojta on 09.12.2020.
//

#include "Item.h"

Item::Item(float hmotnost, std::string nazev, std::string popis)
:Predmet(hmotnost, nazev){
    m_hmotnost = hmotnost;
    m_popis =popis;
    m_nazev = nazev;
}
    float Item::getHmotnost(){
        return m_hmotnost;
}
std::string Item::getNazev(){
    return m_nazev;
}
std::string Item::getPopis(){
    return m_popis;
}

void Item::printInfo() {
    std::cout << "----Item----" << "\n" <<
              "hmotnost: " << getHmotnost() << "\n" <<
              "nazev: " << getNazev() << "\n" <<
              "popis: " << getPopis() << "\n" <<
              "-----------------" << "\n";
}