//
// Created by vojta on 09.12.2020.
//

#include "ZbranANastroj.h"

ZbranANastroj::ZbranANastroj(float hmotnost, std::string nazev, int bonusUtoku)
:Predmet(hmotnost, nazev){
    m_hmotnost = hmotnost;
    m_nazev =nazev;
    m_bonusUtoku = bonusUtoku;
}
float ZbranANastroj::getHmotnost(){
    return m_hmotnost;
}
std::string ZbranANastroj::getNazev(){
    return m_nazev;
}

int ZbranANastroj::getBonusUtoku() {
    return m_bonusUtoku;
}

void ZbranANastroj::printInfo(){
    std::cout << "----Zbran/Nastroj----" << "\n" <<
    "hmotnost: " << getHmotnost() << "\n" <<
    "nazev: " << getNazev() << "\n" <<
    "bonus utoku: " << getBonusUtoku() << "\n" <<
    "-----------------" << "\n";
}