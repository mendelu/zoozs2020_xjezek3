//
// Created by vojta on 09.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_HLAVNIPOSTAVA_H
#define ZOO_PROJEKT_HRA_HLAVNIPOSTAVA_H

#include "Postava.h"

class HlavniPostava : public Postava{
    int m_hp;
    std::string m_jmeno;
    int m_vydrz;
    int m_utok;
public:
    HlavniPostava(std::string jmeno, int hp, int vydrz, int utok);
    std::string getJmeno();
    int getHp();
    int getVydrz();
    int getUtok();
    void printInfo();
    void sebevrazda();
    void uberZivotyLehky(Postava* vedlejsiPostava);
    void uberZivotyTezky(Postava* vedlejsiPostava);


};


#endif //ZOO_PROJEKT_HRA_HLAVNIPOSTAVA_H
