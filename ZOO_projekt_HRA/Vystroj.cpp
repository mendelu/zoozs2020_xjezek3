//
// Created by vojta on 09.12.2020.
//

#include "Vystroj.h"

Vystroj::Vystroj(float hmotnost, std::string nazev, int bonusObrany)
        : Predmet(hmotnost, nazev) {

}

float Vystroj::getHmotnost() {
    return m_hmotnost;
}

std::string Vystroj::getNazev() {
    return m_nazev;
}

int Vystroj::getBonusObrany() {
    return m_bonusObrany;
}

void Vystroj::printInfo() {
    std::cout << "----Vystroj----" << "\n" <<
              "hmotnost: " << getHmotnost() << "\n" <<
              "nazev: " << getNazev() << "\n" <<
              "bonus obrany: " << getBonusObrany() << "\n" <<
              "-----------------" << "\n";
}