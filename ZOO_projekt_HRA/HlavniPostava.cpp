//
// Created by vojta on 09.12.2020.
//

#include "HlavniPostava.h"

HlavniPostava::HlavniPostava(std::string jmeno, int hp, int vydrz, int utok)
:Postava(jmeno, hp, vydrz, utok){
    m_jmeno = jmeno;
    m_hp = hp;
    m_vydrz = vydrz;
    m_utok = utok;

}
std::string HlavniPostava::getJmeno(){
    return m_jmeno;
}
int HlavniPostava::getHp(){
    return m_hp;
}
int HlavniPostava::getVydrz(){
    return m_vydrz;
}
int HlavniPostava::getUtok(){
    return m_utok;
}

void HlavniPostava::printInfo() {
    std::cout << "------" << getJmeno() << "------" << "\n" <<
    "Hp: " << getHp() << "\n" <<
    "Vydrz: " << getVydrz() << "\n" <<
    "Utok: " << getUtok() << "\n" <<
    "|----------------------------------------|" << "\n";
}

void HlavniPostava::sebevrazda(){
    m_hp = 0;
}

void HlavniPostava::uberZivotyLehky(Postava* vedlejsiPostava){
    m_hp -= vedlejsiPostava->getUtok();
}
void HlavniPostava::uberZivotyTezky(Postava* vedlejsiPostava){
    m_hp -= 2*vedlejsiPostava->getUtok();
}


