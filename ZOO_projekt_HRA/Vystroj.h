//
// Created by vojta on 09.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_VYSTROJ_H
#define ZOO_PROJEKT_HRA_VYSTROJ_H

#include "Predmet.h"

class Vystroj : public Predmet{
    int m_bonusObrany;
public:
    Vystroj(float hmotnost, std::string nazev,int bonusObrany);
    float getHmotnost();
    std::string getNazev();
    void printInfo();
    int getBonusObrany();
};


#endif //ZOO_PROJEKT_HRA_VYSTROJ_H
