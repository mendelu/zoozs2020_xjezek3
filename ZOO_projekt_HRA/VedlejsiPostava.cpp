//
// Created by vojta on 09.12.2020.
//

#include <iomanip>
#include "VedlejsiPostava.h"

VedlejsiPostava::VedlejsiPostava(std::string jmeno, int hp, int vydrz, int utok)
:Postava(jmeno, hp, vydrz, utok){
    m_jmeno =jmeno;
    m_hp = hp;
    m_vydrz = vydrz;
    m_utok = utok;
}

int VedlejsiPostava::getUtok() {
    return m_utok;
}
std::string VedlejsiPostava::getJmeno(){
    return m_jmeno;
}
int VedlejsiPostava::getHp() {
    return m_hp;
}
int VedlejsiPostava::getVydrz() {
    return m_vydrz;
}

void VedlejsiPostava::uberZivotyLehky(Postava* hlavniPostava){
    m_hp -= hlavniPostava->getUtok();
}

void VedlejsiPostava::uberZivotyTezky(Postava *hlavniPostava) {
    m_hp -= 2*hlavniPostava->getUtok();
}


void VedlejsiPostava::printInfo() {
    std::cout  << "------" << getJmeno() << "------"  <<  "\n" <<
              "Hp: " << std::right << getHp() << "\n" <<
              "Vydrz: " << std::right << getVydrz() << "\n" <<
              "Utok: " << std::right << getUtok() << "\n" <<
              "|----------------------------------------|" << "\n";
}