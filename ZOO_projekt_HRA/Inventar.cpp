//
// Created by vojta on 09.12.2020.
//

#include "Inventar.h"


Inventar::Inventar(float rozsah) {
    m_rozsah = rozsah;
}

float Inventar::getRozsah() {
    return m_rozsah;
}

float Inventar::getPuvodniRozsah() {
    int puvodniRozsah;
    for (int i = 0; i < m_predmety.size(); i++) {
        m_rozsah += m_predmety.at(i)->getHmotnost();
    }
    puvodniRozsah = m_rozsah;
    return puvodniRozsah;
}


void Inventar::vypisInventar() {
    std::cout << "------INVENTAR------" << "\n";
    for (int i = 0; i < m_predmety.size(); i++) {
        m_predmety.at(i)->printInfo();
    }
    std::cout << "Aktualne volneho mista: " << getRozsah() << "\n";
    if (m_rozsah == 0) {
        std::cout << "\n" << "Mas plny inventar, muzes neco vyhodit." << "\n";
    }

}

void Inventar::pridejPredmet(Predmet *predmet) {
    if (m_rozsah > 0) {
        m_predmety.push_back(predmet);
        m_rozsah -= predmet->getHmotnost();
    } else {
        std::cout << "Mas plny inventar " << "\n" <<
                     "Aktualne ti zbyva: " << getRozsah() << "jednotek mista" << "\n";
    }
}

void Inventar::odeberVse() {
    int rozhodnuti;
    if (!m_predmety.empty()) {
        std::cout << "Opravdu chces vyhodit vse ve svem inventari ?" << "\n" <<
                  "1. Ano" << "\n" <<
                  "2. Ne" << "\n" <<
                  "Rozhodnuti: " << "\n";
        std::cin >> rozhodnuti;
        if (rozhodnuti == 1) {
            getPuvodniRozsah();
            m_predmety.clear();
            std::cout << "Vyhodil jsi vse ze sveho inventare" << "\n";
        } else if (rozhodnuti == 2) {
            std::cout << "Rozohdl jsi se sve veci nechat. " << "\n";
        }

    } else {
        std::cout << "Tvuj inventar je prazdny." << "\n";
    }
}

