//
// Created by vojta on 09.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_ZBRANANASTROJ_H
#define ZOO_PROJEKT_HRA_ZBRANANASTROJ_H

#include "Predmet.h"

class ZbranANastroj : public Predmet {
    float m_hmotnost;
    std::string m_nazev;
    int m_bonusUtoku;
public:
    ZbranANastroj(float hmotnost, std::string nazev, int bonusUtoku);
    float getHmotnost();
    std::string getNazev();
    int getBonusUtoku();
    void printInfo();


};


#endif //ZOO_PROJEKT_HRA_ZBRANANASTROJ_H
