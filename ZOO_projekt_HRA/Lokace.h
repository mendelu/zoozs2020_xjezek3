//
// Created by vojta on 15.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_LOKACE_H
#define ZOO_PROJEKT_HRA_LOKACE_H

#include <vector>
#include "HlavniPostava.h"
#include "VedlejsiPostava.h"
#include "Inventar.h"
#include "Souboj.h"
#include "Pribeh.h"
#include "Predmet.h"
#include "ZbranANastroj.h"
#include "Vystroj.h"
#include "Item.h"

class Lokace {
    std::string m_popis;
    int m_cisloLokace;


public:
    Lokace(int cisloLokace, std::string popis);
    static void vstupDoLokaceUvodni();
    static void vstupDoLokace1(HlavniPostava* hlavniPostava);
    static void sebratNesebratPredmet(Predmet* predmet);
    static void otevriNeotevri1(Predmet* predmet);


};


#endif //ZOO_PROJEKT_HRA_LOKACE_H
