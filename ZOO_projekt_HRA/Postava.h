//
// Created by vojta on 09.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_POSTAVA_H
#define ZOO_PROJEKT_HRA_POSTAVA_H

#include <iostream>

class Postava {
protected:
    std::string m_jmeno;
    int m_hp;
    int m_vydrz;
    int m_utok;
public:
    Postava(std::string jmeno, int hp, int vydrz, int utok);
    virtual std::string getJmeno() = 0;
    virtual int getHp() = 0;
    virtual int getVydrz() = 0;
    virtual int getUtok() = 0;
    virtual void printInfo() = 0;

};


#endif //ZOO_PROJEKT_HRA_POSTAVA_H
