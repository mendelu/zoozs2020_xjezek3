//
// Created by vojta on 09.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_ITEM_H
#define ZOO_PROJEKT_HRA_ITEM_H

#include "Predmet.h"

class Item : public Predmet {
    std::string m_popis;
public:
    Item(float hmotnost, std::string nazev, std::string popis);
    float getHmotnost();
    std::string getNazev();
    std::string getPopis();
    void printInfo();


};


#endif //ZOO_PROJEKT_HRA_ITEM_H
