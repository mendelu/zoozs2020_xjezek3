//
// Created by vojta on 09.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_INVENTAR_H
#define ZOO_PROJEKT_HRA_INVENTAR_H

#include <vector>
#include "Predmet.h"

class Inventar {
    std::vector<Predmet*> m_predmety;
    float m_rozsah;
public:
    Inventar(float rozsah);
    float getRozsah();
    float getPuvodniRozsah();
    void vypisInventar();
    void pridejPredmet(Predmet* predmet);
    void odeberVse();


};


#endif //ZOO_PROJEKT_HRA_INVENTAR_H
