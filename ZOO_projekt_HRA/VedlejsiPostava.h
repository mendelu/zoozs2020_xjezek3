//
// Created by vojta on 09.12.2020.
//

#ifndef ZOO_PROJEKT_HRA_VEDLEJSIPOSTAVA_H
#define ZOO_PROJEKT_HRA_VEDLEJSIPOSTAVA_H

#include "Postava.h"

class VedlejsiPostava : public Postava{
    int m_hp;
    std::string m_jmeno;
    int m_vydrz;
    int m_utok;
public:
    VedlejsiPostava(std::string jmeno, int hp, int vydrz, int utok);
    std::string getJmeno();
    int getHp();
    int getVydrz();
    int getUtok();
    void printInfo();

    void uberZivotyLehky(Postava* hlavniPostava);
    void uberZivotyTezky(Postava* hlavniPostava);

};


#endif //ZOO_PROJEKT_HRA_VEDLEJSIPOSTAVA_H
