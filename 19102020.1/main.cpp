#include <iostream>

using namespace std;

class Student {
    string m_name;
    string m_adress;
    int m_dateOfBirth;

    void setName(string name) {
        if (name != "") {
            m_name = name;
        } else {
            cout << "Student: jmeno nemuze byt prazdne." << endl;
            m_name = "Neni znamo";
        }
    }

    void setAdress(string adress) {
        if (adress != "") {
            m_adress = adress;
        } else {
            cout << "Student: adresa nemuze byt prazdna." << endl;
            m_adress = "Neni zname";
        }
    }

public:
    Student(string name, int dateOfBirth, string adress) {
        m_name = name;
        m_dateOfBirth = dateOfBirth;
        m_adress = adress;
    }

    Student(string name, int dateOfBirth) : Student(name, dateOfBirth, "Neni znamo") {
    }

    string getName() {
        return m_name;
    }

};

class Course {
    string m_nameOfCourse;
    int m_price;

    void setNameOfCourse(string nameOfCourse) {
        if (nameOfCourse != "") {
            m_nameOfCourse = nameOfCourse;
        } else {
            cout << "Kurz: nazev nemuze byt prazdny" << endl;
            m_nameOfCourse = "Neni znam";
        }
    }

public:
    Course(string nameOfCourse, int price) {
        m_nameOfCourse = nameOfCourse;
        m_price = price;
    }

    string getNameOfCourse() {
        return m_nameOfCourse;
    }

};


class Zapis {
    int m_mark;
    Student *m_zapsanyStudent;
    Course *m_zapsanyKurz;

    void setCourse(Course *course) {
        if (course != nullptr) {
            m_zapsanyKurz = course;
        } else {
            cout << "Co to meles, vole? " << endl;
            m_zapsanyKurz = nullptr;
        }
    }

    void setStudent(Student *student) {
        if (student != nullptr) {
            m_zapsanyStudent = student;
        } else {
            cout << "Co to meles, vole? " << endl;
            m_zapsanyStudent = nullptr;
        }
    }

public:
    Zapis(Course *course, Student *student) {
        m_zapsanyStudent = student;
        m_zapsanyKurz = course;
        m_mark = 5;
    }

    void setMark(int mark) {
        if ((mark > 5) or (mark < 1)) {
            cout << "Zapis: znamka musi byt na interbali <1,5>" << endl;
        } else {
            m_mark = mark;
        }
    }

    void printInfo() {
        cout << "Znamka: " << m_mark << endl;
        cout << "Jmeno studenta: " << m_zapsanyStudent->getName() << endl;
        cout << "Nazev kurzu: " << m_zapsanyKurz->getNameOfCourse() << endl;
    }

};

int main() {
    Student* Karel = new Student("Karel", 1989, "Praha");
    Course* matematika = new Course("matematika", 1500);
    Zapis* kareldoMat = new Zapis(matematika, Karel);

    kareldoMat->printInfo();

    delete Karel;
    delete matematika;
    delete kareldoMat;
    return 0;
}
