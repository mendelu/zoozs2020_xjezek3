#include <iostream>
#include <conio.h>
#include <stdlib.h>
#include "MainCharacter.h"
#include "Vystroj.h"
#include "Zbran.h"
#include "Napoj.h"

int main() {


    Zbran* dyka = new Zbran("ocelova Dyka", 5, 15);
    Vystroj* krouzkove = new Vystroj("Krouzkove brneni", 15, 20);
    MainCharacter* mainCharacter = new MainCharacter("Lubos", 100, 25, 20);

    int choice;
    int navyseniUtoku;
    std::cout << "Na zemi jsi nasel blystici se dyku." << "\n" <<
                 "Chces ji sebrat ?" << "\n" <<
                 "1. Sebrat dyku" << "\n" <<
                 "2. Nechat ji lezet na zemi" << "\n" << "\n" <<
                 "Tvoje volba: ";
    std::cin >> choice;
    if(choice == 1){
        mainCharacter->zvysUtok(dyka);
        std::cout << "Sebral jsi ze zeme zbran: " << dyka->getNazev() << "\n"  <<
                     "Tvuj utok ze zvysil na: " << mainCharacter->getUtok() << "\n";

    }
    else if(choice == 2){
        std::cout << "Dyku jsi nechal lezet na zemi." << "\n" <<
                     "Tvuj utok je prave: " << mainCharacter->getUtok() << "\n";
    }





    /*
    system("cls");
    int choiceOne_Path;
    std::cout <<"# What would you like to do?" << std::endl;
    std::cout <<"\t >> Enter '1' to follow the Chief ?" << std::endl;
    std::cout <<"\t >> Enter '2' to find your own path?" << std::endl;
    retry:
    std::cout << "\nEnter your choice: ";
    std::cin >> choiceOne_Path;
    if(choiceOne_Path == 1){
        std::cout << "\n!!!-----------------Chapter One:"
                "Escape--------------------!!!" << std::endl;
        std::cout << "\nYou: Where are we going ?" << std::endl;
        std::cout <<"Chief: Soon you will know. Just follow me." << std::endl;
        std::cout <<"# You run behind the chief." << std::endl;
    }
    else if(choiceOne_Path == 2){
        std::cout << "\n!!!-----------------Chapter One:"
                "Escape--------------------!!!" << endl;
        std::cout <<"\nYou: I am going to find a way out!" << endl;
        std::cout <<"Chief: You are insane. You will get killed out there." << std::endl;
        std::cout <<"You: I have my secrets and I know my way out." << std::endl;
        std::cout<<"# You jump over the nearby broken fence" << std::endl;
        std::cout<<"# and run off towards the City Wall." << std::endl;
    }
    else{
        std::cout << "You are doing it wrong, warrior! Press either '1' or '2', nothing else!" << std::endl;
        goto retry;
    }
    std::cout << "\n-----------------Press any key to continue-------------" << std::endl;
    _getch();
     */
    return 0;
}
