//
// Created by vojta on 06.12.2020.
//

#include "Napoj.h"

Napoj::Napoj(std::string nazev, int hmotnost, int bonusZivota)
:Predmet(nazev, hmotnost){
    m_nazev = nazev;
    m_hmotnost = hmotnost;
    m_bonusZivota = bonusZivota;
}
std::string Napoj::getNazev(){
    return m_nazev;
}
int Napoj::getHmotnost(){
    return m_hmotnost;
}
int Napoj::getBonusZivota(){
    return m_bonusZivota;
}

