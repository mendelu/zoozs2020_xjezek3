//
// Created by vojta on 06.12.2020.
//

#ifndef POKUSHRA_ZBRAN_H
#define POKUSHRA_ZBRAN_H

#include "Predmet.h"

class Zbran : public Predmet {
    std::string m_nazev;
    int m_hmotnost;
    int m_bonusUtoku;
public:
    Zbran(std::string nazev, int hmotnost, int bonusUtoku);
    std::string getNazev();
    int getHmotnost();
    int getBonusUtoku();



};


#endif //POKUSHRA_ZBRAN_H
