//
// Created by vojta on 06.12.2020.
//

#ifndef POKUSHRA_VYSTROJ_H
#define POKUSHRA_VYSTROJ_H

#include "Predmet.h"

class Vystroj : public Predmet {
    std::string m_nazev;
    int m_hmotnost;
    int m_bonusObrany;
public:
    Vystroj(std::string nazev, int hmotnost, int bonusObrany);
    std::string getNazev();
    int getHmotnost();
    int getBonusObrany();
};


#endif //POKUSHRA_VYSTROJ_H
