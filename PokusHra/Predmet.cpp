//
// Created by vojta on 06.12.2020.
//

#include "Predmet.h"

Predmet::Predmet(std::string nazev, int hmotnost) {
    m_nazev = nazev;
    m_hmotnost = hmotnost;
}
int Predmet::getHmotnost() {
    return m_hmotnost;

}
std::string Predmet::getNazev() {
    return m_nazev;
}