//
// Created by vojta on 06.12.2020.
//

#ifndef POKUSHRA_NAPOJ_H
#define POKUSHRA_NAPOJ_H

#include <iostream>
#include <vector>
#include "Predmet.h"

class Napoj : public Predmet{
protected:
    std::string m_nazev;
    int m_hmotnost;
    int m_bonusZivota;
    std::vector<Napoj*> m_napoje;

public:
    Napoj(std::string nazev, int hmotnost, int bonusZivota);
    std::string getNazev();
    int getHmotnost();
    int getBonusZivota();
    void vypijNapoj(Napoj* napoj);
    void zahodNapoj(Napoj* napoj);
    void zahodPosledniNapoj();

};


#endif //POKUSHRA_NAPOJ_H
