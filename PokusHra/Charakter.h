//
// Created by vojta on 06.12.2020.
//

#ifndef POKUSHRA_CHARAKTER_H
#define POKUSHRA_CHARAKTER_H

#include <iostream>

class Charakter {
protected:
    std::string m_jmeno;
    int m_hp;
    int m_utok;
    int m_obrana;
public:
    Charakter(std::string jmeno, int hp, int utok, int obrana);
    virtual std::string getJmeno();
    virtual int getHp();
    virtual int getUtok();
    virtual int getObrana();
};


#endif //POKUSHRA_CHARAKTER_H
