//
// Created by vojta on 06.12.2020.
//

#ifndef POKUSHRA_MAINCHARACTER_H
#define POKUSHRA_MAINCHARACTER_H

#include <vector>
#include "Charakter.h"
#include "Vystroj.h"
#include "Zbran.h"
#include "Napoj.h"


class MainCharacter : public Charakter {
    std::string m_jmeno;
    int m_hp;
    int m_utok;
    int m_obrana;
    Vystroj* m_vystroj;
    Zbran* m_zbran;
    std::vector <Napoj*> m_napoje;
public:
    MainCharacter(std::string jmeno, int hp, int utok, int obrana);
    std::string getJmeno();
    int getHp();
    int getUtok();
    int getObrana();
    void VypijNapoj(Napoj* napoj);
    void zvysUtok(Zbran * novaZbran);




};


#endif //POKUSHRA_MAINCHARACTER_H
