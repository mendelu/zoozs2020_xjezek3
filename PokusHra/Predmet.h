//
// Created by vojta on 06.12.2020.
//

#ifndef POKUSHRA_PREDMET_H
#define POKUSHRA_PREDMET_H

#include <iostream>

class Predmet {
protected:
    std::string m_nazev;
    int m_hmotnost;
public:
    Predmet(std::string nazev, int hmotnost);
    virtual int getHmotnost();
    virtual std::string getNazev();





};


#endif //POKUSHRA_PREDMET_H
