//
// Created by vojta on 06.12.2020.
//

#include "Vystroj.h"
Vystroj::Vystroj(std::string nazev, int hmotnost, int bonusObrany)
:Predmet(nazev, hmotnost){
    m_nazev = nazev;
    m_hmotnost = hmotnost;
    m_bonusObrany = bonusObrany;
}
std::string Vystroj::getNazev(){
    return m_nazev;
}
int Vystroj::getHmotnost(){
    return m_hmotnost;
}
int Vystroj::getBonusObrany(){
    return m_bonusObrany;
}
