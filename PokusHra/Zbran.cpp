//
// Created by vojta on 06.12.2020.
//

#include "Zbran.h"


Zbran::Zbran(std::string nazev, int hmotnost, int bonusUtoku)
:Predmet(nazev, hmotnost){
    m_nazev = nazev;
    m_hmotnost = hmotnost;
    m_bonusUtoku = bonusUtoku;
}
std::string Zbran::getNazev(){
    return m_nazev;
}
int Zbran::getHmotnost(){
    return m_hmotnost;
}
int Zbran::getBonusUtoku(){
    return m_bonusUtoku;
}