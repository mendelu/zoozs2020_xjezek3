//
// Created by vojta on 06.12.2020.
//

#include "Charakter.h"

Charakter::Charakter(std::string jmeno, int hp, int utok, int obrana){
    m_jmeno = jmeno;
    m_hp = hp;
    m_utok = utok;
    m_obrana = obrana;

}
std::string Charakter::getJmeno(){
    return m_jmeno;
}
int Charakter::getHp(){
    return m_hp;
}
int Charakter::getUtok(){
    return m_utok;
}
int Charakter::getObrana(){
    return m_obrana;
}