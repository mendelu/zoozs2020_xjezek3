//
// Created by vojta on 06.12.2020.
//

#include "MainCharacter.h"

MainCharacter::MainCharacter(std::string jmeno, int hp, int utok, int obrana)
:Charakter(jmeno, hp, utok, obrana){
    m_hp = hp;
    m_utok = utok;
    m_obrana = obrana;
    m_zbran = nullptr;
    m_vystroj = nullptr;


}
std::string MainCharacter::getJmeno(){
    return m_jmeno;
}
int MainCharacter::getHp(){
    return m_hp;
}
int MainCharacter::getUtok(){
    return m_utok;
}
int MainCharacter::getObrana(){
    return m_obrana;
}
void MainCharacter::VypijNapoj(Napoj* napoj){
    m_hp += napoj->getBonusZivota();
}

void MainCharacter::zvysUtok(Zbran* novaZbran) {
    m_utok += novaZbran->getBonusUtoku();

}