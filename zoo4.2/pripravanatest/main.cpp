#include <iostream>

using namespace std;

class Passenger {
    string m_name;
    float m_weight;
    float m_luggageWeight;

public:
    Passenger(string Name, float Weight, float LuggageWeight) {
        m_name = Name;
        m_weight = Weight;
        m_luggageWeight = LuggageWeight;
    }

// Konstuktor pro pripad, že nevím weigh, pak je weight == 70
    Passenger(string Name, float LuggageWeight) : Passenger(Name, 70.0, LuggageWeight) {

    }

    // Vrátí celkovou weight passengera
    float getTotalWeight() {
        return m_weight + m_luggageWeight;
    }

    //Vrátí Name
    string getName() {
        return m_name;
    }

    void printInfo(){
        cout << "Udaje o pasazerovi: " << m_name << ", " << m_weight << ", " << m_luggageWeight << "."
        << endl;
    }
};

class Elevator {
    float m_maxWeight;
    float m_currentWeight;
    int m_amountOfPassengers;

public:
    Elevator() {
        m_maxWeight = 200.0;
        m_currentWeight = 0.0;
        m_amountOfPassengers = 0;
    }

    void addPassenger(Passenger *passenger) {
        float newWeight = m_currentWeight + passenger->getTotalWeight();
        if (newWeight <= m_maxWeight) {
            m_currentWeight = newWeight;
            m_amountOfPassengers++;

        } else {
            cout << "Pasazera" << passenger->getName() << "není mozne jiz pridat!!!"
                 << endl;
        }
    }

    void removePassenger(Passenger*passenger){
    }

    void printInfo(){
        cout << "Aktualni hmotnost: " << m_currentWeight << endl;
        cout << "Prumerna vaha: " << calculateAverage() << endl;
        cout << "Pocet pasazeru: " << m_amountOfPassengers << endl;
    }

private:
    float calculateAverage(){
        return (m_currentWeight/m_amountOfPassengers);
    }
};

int main() {
    Elevator * vytah = new Elevator();
    vytah -> printInfo();
    Passenger* passenger1 = new Passenger("Karel", 70.0, 4.0);
    Passenger* passenger2 = new Passenger("Pepa", 4.0);

    vytah->addPassenger(passenger1);
    vytah->addPassenger(passenger2);
    vytah -> printInfo();
    passenger2->printInfo();

    delete vytah;
    return 0;
}