#include <iostream>

using namespace std;

class Lektvar {
    string m_jmeno;
    int m_bonusKZivotum;
public:
    Lektvar(string jmeno, int bonusKZivotum) {
        m_jmeno = jmeno;
        m_bonusKZivotum = bonusKZivotum;
    }

    string getJmeno() {
        return m_jmeno;
    }

    int getBonusKZivotum() {
        return m_bonusKZivotum;
    }


};

class Zbran {
private:
    string m_jmeno;
    int m_bonusKUtoku;
public:
    Zbran(string jmeno, int bonusKUtoku) {
        m_jmeno = jmeno;
        m_bonusKUtoku = bonusKUtoku;
    }

    string getJmeno() {
        return m_jmeno;
    }

    int getBonusZbrane() {
        return m_bonusKUtoku;
    }


};

class Hrdina {
private:
    string m_jmeno;
    int m_pocetZivotu;
    int m_sila;
    Zbran *m_zbran;

public:
    Hrdina(string jmeno, int sila) {
        m_jmeno = jmeno;
        m_sila = sila;
        m_pocetZivotu = 100;
        m_zbran = nullptr;
    }

    string getJmeno() {
        return m_jmeno;
    }

    int getPocetZivotu() {
        return m_pocetZivotu;
    }

    int getSila() {
        if (m_zbran == nullptr) {
            return m_sila;
        } else {
            return m_sila + m_zbran->getBonusZbrane();
        }
    }

    void uberZivot(int oKolik) {
        m_pocetZivotu -= oKolik;
        if (m_pocetZivotu <= 0) {
            cout << "Hrdina " << m_jmeno << " je mrtev" << endl;
        }
    }

    void printInfo() {
        cout << "Jmeno: " << getJmeno() << endl;
        cout << "Zivoty: " << getPocetZivotu() << endl;
        cout << "Sila: " << getSila() << endl;
        cout << " " << endl;
    }

    void zautoc(Hrdina *nepritel) {
        int kolikUbrat = getSila();
        nepritel->uberZivot(kolikUbrat);
    }

    void seberZbran(Zbran *zbranCoJsemSebral) {
        m_zbran = zbranCoJsemSebral;

    }

    void zahodZbran() {
        m_zbran = nullptr;
    }

    void vypijLektvar(Lektvar *lektvarCoChciVypit){
        m_pocetZivotu += lektvarCoChciVypit->getBonusKZivotum();
    }


    };


int main() {
    Zbran *excalibur = new Zbran("Excalibur", 10);
    Zbran *bastard = new Zbran("bastard", 40);
    Hrdina *artus = new Hrdina("Artus", 20);
    Hrdina *tom = new Hrdina("Tom", 40);



    delete excalibur;
    delete bastard;
    delete artus;
    delete tom;
    return 0;
}
